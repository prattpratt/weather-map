### What is this repository for? ###

This is just sample Android app that uses :

* Google Maps fragment to show map of the Earth filled w/ weather pins
* List View fragment to show pins list w/ weather info
* Nav drawer fragment to switch between List view and Map view
* Background Service to handle weather requests
* SQLite DB to store weather details
* The app is compatible w/ Android 2.3.3+ (API 10+)

### How do I get set up? ###

* Get *Android SDK*, install latest Android Platform and some extras:
	* *Android Support Repository*
	* *Android Support Library*
	* *Google Play services*
	* *Google Repository*
* Use *IDEA (Android-Gradle)* or *Eclipse (ADT/Gradle)* or *Gradle* or *Maven* to build the project
* Before using *Eclipse (ADT)* have a look at [Support Library Setup](http://developer.android.com/tools/support-library/setup.html) and [Setting Up Google Play Services](https://developer.android.com/google/play-services/setup.html)
* Before using *Eclipse (Gradle)* you need to install [Gradle Integration for Eclipse](http://marketplace.eclipse.org/content/gradle-integration-eclipse-37-43) plugin
* Before using *Maven* have a look at [android-maven-plugin](https://github.com/simpligility/android-maven-plugin) and [maven-android-sdk-deployer](https://github.com/simpligility/maven-android-sdk-deployer)
* Run using default AVD from SDK or Genymotion device or real Android device

### Screenshots ###
![Map View](https://bitbucket.org/repo/M64R68/images/1732549090-map_view.png)
![Nav Drawer](https://bitbucket.org/repo/M64R68/images/3655690330-nav-drawer.png)
![Pins View](https://bitbucket.org/repo/M64R68/images/1338295686-pins-view.png)

### Who do I talk to? ###

* @prattpratt