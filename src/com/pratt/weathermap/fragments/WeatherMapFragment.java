package com.pratt.weathermap.fragments;

import android.Manifest;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pratt.weathermap.R;
import com.pratt.weathermap.data.WeatherPin;
import com.pratt.weathermap.data.WeatherProvider;
import com.pratt.weathermap.database.WeatherPinsDbHelper;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * @author Siarhei_Valiulik
 * @created 1/13/2015
 */
public class WeatherMapFragment extends SupportMapFragment implements
        OnMapReadyCallback, OnMapLongClickListener, OnMarkerClickListener,
        WeatherProvider.WeatherListener, EasyPermissions.PermissionCallbacks {

    private static final String MAP_OPTIONS = "mapOptions";
    private static final int RC_LOCATION = 135;

    private Bitmap mUnknownWeatherIcon;

    /** The SQLite DB helper. */
    private WeatherPinsDbHelper mDbHelper;

    /** The list of markers on the map in order to remove and to show info window. */
    private List<Marker> mMarkers;

    /** The list position of marker on the map that shows info window. */
    private int mSelectedMarkerPosition;

    /** The camera position on the map. */
    private CameraPosition mCameraPosition;

    /** A weak ref to Google Map UI object */
    private WeakReference<GoogleMap> mGoogleMap;

    private WeatherProvider mWeatherProvider;

    public static WeatherMapFragment newInstance() {
        return new WeatherMapFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getMapAsync(this);
        setRetainInstance(true);

        mUnknownWeatherIcon = BitmapFactory.decodeResource(getResources(), android.R.drawable.ic_menu_help);
        mDbHelper = new WeatherPinsDbHelper(getActivity());
        mMarkers = new ArrayList<>();
        mWeatherProvider = new WeatherProvider(getString(R.string.openweather_api_key), this);
    }

    @Override
    public void onPause() {
        super.onPause();
        // save camera position
        if (mGoogleMap != null && mGoogleMap.get() != null) {
            mCameraPosition = mGoogleMap.get().getCameraPosition();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // try to save camera position
        if (savedInstanceState != null) {
            mCameraPosition = savedInstanceState.getParcelable(MAP_OPTIONS);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // try to restore camera position
        outState.putParcelable(MAP_OPTIONS, mCameraPosition);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        OnFragmentAttached listener = (OnFragmentAttached) activity;
        listener.onFragmentAttached(this);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mGoogleMap = new WeakReference<>(map);

        // move camera to the same place
        if (mCameraPosition != null) {
            map.animateCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));
        }

        // add all map markers
        for (WeatherPin pin : mDbHelper.getMapPins()) {
            addMarkerOnMap(pin.getLatLng(), pin.getTemperature(), pin.getWeatherIcon());
        }
        // show info windows for selected marker
        if (!mMarkers.isEmpty()) {
            mMarkers.get(mSelectedMarkerPosition).showInfoWindow();
        }

        map.setOnMapLongClickListener(this);
        map.setOnMarkerClickListener(this);

        setupGoogleMap();
    }

    @SuppressWarnings({"MissingPermission"})
    @AfterPermissionGranted(RC_LOCATION)
    private void setupGoogleMap() {
        String[] perms = { Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION };

        if (EasyPermissions.hasPermissions(getContext(), perms)) {
            if (mGoogleMap != null && mGoogleMap.get() != null) {
                GoogleMap map = mGoogleMap.get();
                map.setMyLocationEnabled(true);
                map.getUiSettings().setZoomControlsEnabled(true);
            }
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, getString(R.string.location_rationale), RC_LOCATION, perms);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        Toast.makeText(getContext(), getString(R.string.start_weather), Toast.LENGTH_SHORT).show();
        mWeatherProvider.getWeather(latLng);
    }

    /**
     * Adds a marker on the map and make info window visible
     *
     * @param latLng      the latitude & longitude
     * @param temperature the temperature
     * @param weatherIcon the weather icon
     */
    private void addMarkerOnMap(LatLng latLng, String temperature, Bitmap weatherIcon) {
        if (mGoogleMap != null && mGoogleMap.get() != null) {
            BitmapDescriptor bitmapDesc = BitmapDescriptorFactory.fromBitmap(resizeBitmap(weatherIcon));
            MarkerOptions options = new MarkerOptions().position(latLng).title(temperature).icon(bitmapDesc);
            Marker marker = mGoogleMap.get().addMarker(options);
            marker.showInfoWindow();
            // add marker to the list
            mMarkers.add(marker);
        }
    }

    /**
     * Resize bitmap.
     *
     * @param b the bitmap to process
     * @return the resized bitmap
     */
    private Bitmap resizeBitmap(Bitmap b) {
        int px = (int) getResources().getDimension(R.dimen.weather_icon_width_map);
        return Bitmap.createScaledBitmap(b, px, px, false);
    }

    /**
     * Removes the marker from map.
     *
     * @param position the position in the list
     */
    public void removeMarkerFromMap(int position) {
        mMarkers.get(position).remove();
        // remove marker from the list
        mMarkers.remove(position);
        mSelectedMarkerPosition = 0;
    }

    /**
     * Update camera position and selected marker after click on pin event
     *
     * @param position the pin position in the list
     */
    public void updateCameraPositionAndSelectedMarker(int position) {
        LatLng latLng = mMarkers.get(position).getPosition();
        mCameraPosition = CameraPosition.fromLatLngZoom(latLng, 5);
        mSelectedMarkerPosition = position;
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        mSelectedMarkerPosition = mMarkers.indexOf(marker);
        return false;
    }

    @Override
    public void onWeatherLoaded(final LatLng latLng, final String temperature, Bitmap weatherIcon) {
        final FragmentActivity activity = getActivity();
        if (activity == null) {
            return;
        }

        final Bitmap icon = (weatherIcon == null) ? mUnknownWeatherIcon : weatherIcon;

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, getString(R.string.stop_weather), Toast.LENGTH_SHORT).show();
                addMarkerOnMap(latLng, temperature, icon);
                long timestamp = System.currentTimeMillis();
                long rowId = mDbHelper.addMapPin(latLng, temperature, icon, timestamp);
                if (rowId == -1) {
                    Toast.makeText(activity, getString(R.string.error_add_map_pin), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        // no-op
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        // (Optional) Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        // This will display a dialog directing them to enable the permission in app settings.
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this, getString(R.string.rationale_ask_again))
                    .setTitle(getString(R.string.title_settings_dialog))
                    .setPositiveButton(getString(R.string.setting))
                    .setNegativeButton(getString(android.R.string.cancel), null /* click listener */)
                    .setRequestCode(RC_LOCATION)
                    .build()
                    .show();
        }
    }
}
