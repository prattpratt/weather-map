package com.pratt.weathermap.fragments;

public interface OnPinsListItemCallbacks {

    void onPinsListItemClicked(int position);

    void onPinListItemDeleted(int position);
}
