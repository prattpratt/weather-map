package com.pratt.weathermap.fragments;

import android.support.v4.app.Fragment;

/**
 * @author Siarhei_Valiulik
 * @created 1/13/2015
 */
public interface OnFragmentAttached {

    void onFragmentAttached(Fragment fragment);
}
