package com.pratt.weathermap.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.widget.CursorAdapter;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.lang.ref.WeakReference;

import com.pratt.weathermap.R;
import com.pratt.weathermap.adapters.PinsListViewCursorAdapter;
import com.pratt.weathermap.database.WeatherPinsDbHelper;

/**
 * A pins list fragment.
 */
public class PinsListFragment extends ListFragment implements AdapterView.OnItemLongClickListener {

    /** The SQLite DB helper. */
    private WeatherPinsDbHelper mDbHelper;

    /**
     * A weak ref to the current callbacks instance (the Activity).
     * Callbacks are triggered on clicks and long-clicks on pins list item.
     */
    private WeakReference<OnPinsListItemCallbacks> mCallbacks;

    /** The list view adapter to map data between pins list and SQLite DB. */
    private PinsListViewCursorAdapter mPinsListViewAdapter;

    public static PinsListFragment newInstance() {
        return new PinsListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDbHelper = new WeatherPinsDbHelper(getActivity());
        mPinsListViewAdapter = new PinsListViewCursorAdapter(
                getActivity(),
                mDbHelper.getCursor(),
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

        setListAdapter(mPinsListViewAdapter);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getListView().setOnItemLongClickListener(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallbacks = new WeakReference<>((OnPinsListItemCallbacks) activity);
        OnFragmentAttached listener = (OnFragmentAttached) activity;
        listener.onFragmentAttached(this);
    }

    @Override
    public void onListItemClick(ListView parent, View view, int position, long id) {
        mCallbacks.get().onPinsListItemClicked(position);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.dialog_delete_title)
                .setMessage(String.format(getString(R.string.dialog_delete_message), position + 1))
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new AlertDialog.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        long pinId = mPinsListViewAdapter.getItemId(position);
                        int deletedRows = mDbHelper.deleteMapPin(pinId);
                        if (deletedRows != 1) {
                            Toast.makeText(getActivity(), getString(R.string.error_delete_map_pin), Toast.LENGTH_LONG)
                                    .show();
                            return;
                        }
                        mCallbacks.get().onPinListItemDeleted(position);
                        mPinsListViewAdapter.changeCursor(mDbHelper.getCursor());
                    }
                })
                .show();
        return true;
    }
}
