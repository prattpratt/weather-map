package com.pratt.weathermap.data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Locale;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Class to load weather from the weather service.
 *
 * @author Siarhei_Valiulik
 * @created 10/28/2016
 */
public final class WeatherProvider {
    private static final String TAG = WeatherProvider.class.getSimpleName();

    private OkHttpClient mHttpClient = new OkHttpClient.Builder().build();

    private String mApiKey;
    private WeatherListener mListener;

    public WeatherProvider(String apiKey, WeatherListener listener) {
        mApiKey = apiKey;
        mListener = listener;
    }

    /**
     * Performs weather request in the background thread and return result to listener.
     *
     * @param latLng the latitude & longitude
     */
    public void getWeather(final LatLng latLng) {
        HttpUrl weatherUrl = new HttpUrl.Builder()
                .scheme("http")
                .host("api.openweathermap.org")
                .addPathSegments("data/2.5/weather")
                .addQueryParameter("units", "metric")
                .addQueryParameter("appid", mApiKey)
                .addQueryParameter("lat", String.valueOf(latLng.latitude))
                .addQueryParameter("lon", String.valueOf(latLng.longitude))
                .build();

        Request weatherRequest = new Request.Builder()
                .url(weatherUrl)
                .get()
                .build();

        mHttpClient.newCall(weatherRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e(TAG, "Error retrieving weather", e);

                if (mListener != null) {
                    mListener.onWeatherLoaded(latLng, "N/A", null);
                }
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String string = response.body().string();
                Log.d(TAG, "Weather response: " + string);

                Bitmap weatherIcon = null;
                String temperature = "N/A";

                try {
                    JSONObject jsonObject = new JSONObject(string);

                    if (jsonObject.has("main")) {

                        Double tempCelsius = jsonObject.getJSONObject("main").getDouble("temp");
                        temperature = String.format(Locale.getDefault(), "%+.0f °C", tempCelsius);

                        String iconId = jsonObject.getJSONArray("weather").getJSONObject(0).getString("icon");
                        String imageUrl = "http://openweathermap.org/img/w/" + iconId + ".png";
                        weatherIcon = BitmapFactory.decodeStream((InputStream) new URL(imageUrl).getContent());
                    } else {
                        String message = jsonObject.getString("message");
                        Log.e(TAG, "Weather request error: " + message);
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "Error parsing weather response", e);
                }

                if (mListener != null) {
                    mListener.onWeatherLoaded(latLng, temperature, weatherIcon);
                }
            }
        });
    }

    public interface WeatherListener {

        void onWeatherLoaded(LatLng latLng, String temperature, Bitmap weatherIcon);
    }
}
