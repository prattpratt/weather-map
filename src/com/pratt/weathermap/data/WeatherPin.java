package com.pratt.weathermap.data;

import android.graphics.Bitmap;

import java.io.Serializable;

import com.google.android.gms.maps.model.LatLng;

/**
 * @author Sergey.Valiulik
 * @created 1/16/2015
 */
public final class WeatherPin implements Serializable {

    private static final long serialVersionUID = 7635674247469312482L;

    private long id;
    private LatLng latLng;
    private String temperature;
    private Bitmap weatherIcon;
    private long timestamp;

    public WeatherPin() {
    }

    public WeatherPin(LatLng latLng, String temperature, Bitmap weatherIcon, long timestamp) {
        this.latLng = latLng;
        this.temperature = temperature;
        this.weatherIcon = weatherIcon;
        this.timestamp = timestamp;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public Bitmap getWeatherIcon() {
        return weatherIcon;
    }

    public void setWeatherIcon(Bitmap weatherIcon) {
        this.weatherIcon = weatherIcon;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
