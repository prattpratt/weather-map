package com.pratt.weathermap.adapters;

import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.google.android.gms.maps.model.LatLng;
import com.pratt.weathermap.R;
import com.pratt.weathermap.data.WeatherPin;
import com.pratt.weathermap.database.WeatherPinsDbHelper;

public class PinsListViewCursorAdapter extends CursorAdapter {

    private LayoutInflater mInflater;

    /**
     * The View Holder class
     */
    private static class ViewHolder {
        ImageView weatherIcon;
        TextView temp;
        TextView lat;
        TextView lon;
        TextView timestamp;
    }

    public PinsListViewCursorAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View rowView = mInflater.inflate(R.layout.weather_pin_item, parent, false);
        ViewHolder holder = new ViewHolder();
        // get all child views and store them in view holder
        holder.weatherIcon = (ImageView) rowView.findViewById(R.id.weatherIcon);
        holder.temp = (TextView) rowView.findViewById(R.id.temperature);
        holder.lat = (TextView) rowView.findViewById(R.id.latitude);
        holder.lon = (TextView) rowView.findViewById(R.id.longitude);
        holder.timestamp = (TextView) rowView.findViewById(R.id.timestamp);
        rowView.setTag(holder);
        return rowView;
    }

    @Override
    public void bindView(View rowView, Context context, Cursor cursor) {
        // read all columns data from cursor
        WeatherPin mapPin = WeatherPinsDbHelper.getMapPin(cursor);

        // use cached child views from view holder
        ViewHolder holder = (ViewHolder) rowView.getTag();

        // set weather icon
        holder.weatherIcon.setImageBitmap(mapPin.getWeatherIcon());
        // set temperature
        holder.temp.setText(mapPin.getTemperature());
        // set formatted lat & lon
        LatLng latLng = mapPin.getLatLng();
        holder.lat.setText(formatGeoCoordinate(latLng.latitude));
        holder.lon.setText(formatGeoCoordinate(latLng.longitude));
        // set formatted time-stamp
        DateFormat sdf = new SimpleDateFormat("dd/MMM/yy", Locale.getDefault());
        holder.timestamp.setText(sdf.format(new Date(mapPin.getTimestamp())));
    }

    /**
     * Gets the formatted geographic coordinate.
     *
     * @param coordinate latitude or longitude
     * @return the formatted geo coordinate
     */
    private static String formatGeoCoordinate(double coordinate) {
        String[] dms = Location.convert(coordinate, Location.FORMAT_SECONDS).split(":");
        String degrees = dms[0];
        String minutes = dms[1];
        String seconds = dms[2];
        return String.format("%s° %s′ %s″", degrees, minutes, seconds);
    }

}
