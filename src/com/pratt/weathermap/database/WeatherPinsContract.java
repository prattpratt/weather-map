package com.pratt.weathermap.database;

import android.provider.BaseColumns;

/**
 * @author Sergey.Valiulik
 * @created 1/16/2015
 */
public final class WeatherPinsContract {

    private WeatherPinsContract() {
    }

    /* Inner class that defines the table contents */
    public static abstract class WeatherPinEntry implements BaseColumns {
        public static final String TABLE_NAME = "weather";

        public static final String COLUMN_PIN_ID = _ID;
        public static final String COLUMN_LATITUDE = "latitude";
        public static final String COLUMN_LONGITUDE = "longitude";
        public static final String COLUMN_TEMPERATURE = "temperature";
        public static final String COLUMN_WEATHER_ICON = "weather_icon";
        public static final String COLUMN_TIMESTAMP = "timestamp";
    }
}