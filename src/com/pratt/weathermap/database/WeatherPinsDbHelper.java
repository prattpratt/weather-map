package com.pratt.weathermap.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.model.LatLng;
import com.pratt.weathermap.data.WeatherPin;

import static com.pratt.weathermap.database.WeatherPinsContract.WeatherPinEntry.COLUMN_LATITUDE;
import static com.pratt.weathermap.database.WeatherPinsContract.WeatherPinEntry.COLUMN_LONGITUDE;
import static com.pratt.weathermap.database.WeatherPinsContract.WeatherPinEntry.COLUMN_PIN_ID;
import static com.pratt.weathermap.database.WeatherPinsContract.WeatherPinEntry.COLUMN_TEMPERATURE;
import static com.pratt.weathermap.database.WeatherPinsContract.WeatherPinEntry.COLUMN_TIMESTAMP;
import static com.pratt.weathermap.database.WeatherPinsContract.WeatherPinEntry.COLUMN_WEATHER_ICON;
import static com.pratt.weathermap.database.WeatherPinsContract.WeatherPinEntry.TABLE_NAME;

/**
 * @author Sergey.Valiulik
 * @created 1/15/2015
 */
public final class WeatherPinsDbHelper extends SQLiteOpenHelper {

    private static final String LAT_LON_SELECTION = COLUMN_LATITUDE + " = ? AND " + COLUMN_LONGITUDE + " = ?";
    private static final String PIN_ID_SELECTION = COLUMN_PIN_ID + " = ?";

    private static final String INT_PRIMARY = " INTEGER PRIMARY KEY AUTOINCREMENT";
    private static final String INT_TYPE = " INTEGER";
    private static final String DOUBLE_TYPE = " REAL";
    private static final String TEXT_TYPE = " TEXT";
    private static final String IMAGE_TYPE = " BLOB";
    private static final String COMMA_SEP = ",";

    private static final String SQL_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
            COLUMN_PIN_ID + INT_PRIMARY + COMMA_SEP +
            COLUMN_LATITUDE + DOUBLE_TYPE + COMMA_SEP +
            COLUMN_LONGITUDE + DOUBLE_TYPE + COMMA_SEP +
            COLUMN_TEMPERATURE + TEXT_TYPE + COMMA_SEP +
            COLUMN_WEATHER_ICON + IMAGE_TYPE + COMMA_SEP +
            COLUMN_TIMESTAMP + INT_TYPE +
            " )";

    private static final String SQL_DELETE_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    private static final String SQL_SELECT_ALL = "SELECT * FROM " + TABLE_NAME;

    // If you change the database schema, you must increment the database version.
    private static final int DB_VERSION = 2;
    private static final String DB_NAME = "WeatherPins.db";

    private static final String[] ALL_COLUMNS = new String[] {
            COLUMN_PIN_ID,
            COLUMN_LATITUDE,
            COLUMN_LONGITUDE,
            COLUMN_TEMPERATURE,
            COLUMN_WEATHER_ICON,
            COLUMN_TIMESTAMP
    };

    public WeatherPinsDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_TABLE);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    /**
     * Creates a bitmap from a byte array
     *
     * @param bitmapArray the bitmap byte array
     * @return the bitmap object
     */
    private static Bitmap createBitmap(byte[] bitmapArray) {
        return BitmapFactory.decodeByteArray(bitmapArray, 0, bitmapArray.length);
    }

    /**
     * Creates a byte array from {@link Bitmap} object
     *
     * @param bitmap the bitmap
     * @return the byte array
     */
    private static byte[] createByteArray(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }

    /**
     * Add a single pin to DB.
     *
     * @param latLng      the latitude and longitude of the pin
     * @param temperature the temperature
     * @param weatherIcon the weather icon
     * @param timestamp   the timestamp
     * @return the row ID of the newly inserted row, or -1 if an error occurred
     */
    public long addMapPin(LatLng latLng, String temperature, Bitmap weatherIcon, long timestamp) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_LATITUDE, latLng.latitude);
        values.put(COLUMN_LONGITUDE, latLng.longitude);
        values.put(COLUMN_TEMPERATURE, temperature);
        values.put(COLUMN_WEATHER_ICON, createByteArray(weatherIcon));
        values.put(COLUMN_TIMESTAMP, timestamp);

        long rowId = db.insert(TABLE_NAME, null, values);
        close();
        return rowId;
    }

    /**
     * Gets the pin on current cursor position
     *
     * @param cursor the cursor
     * @return the weather pin
     */
    public static WeatherPin getMapPin(Cursor cursor) {
        int idIndex = cursor.getColumnIndexOrThrow(COLUMN_PIN_ID);
        int latIndex = cursor.getColumnIndexOrThrow(COLUMN_LATITUDE);
        int lonIndex = cursor.getColumnIndexOrThrow(COLUMN_LONGITUDE);
        int tempIndex = cursor.getColumnIndexOrThrow(COLUMN_TEMPERATURE);
        int iconIndex = cursor.getColumnIndexOrThrow(COLUMN_WEATHER_ICON);
        int timestampIndex = cursor.getColumnIndexOrThrow(COLUMN_TIMESTAMP);

        double latitude = cursor.getDouble(latIndex);
        double longitude = cursor.getDouble(lonIndex);

        WeatherPin pin = new WeatherPin();
        pin.setId(cursor.getLong(idIndex));
        pin.setLatLng(new LatLng(latitude, longitude));
        pin.setTemperature(cursor.getString(tempIndex));
        pin.setWeatherIcon(createBitmap(cursor.getBlob(iconIndex)));
        pin.setTimestamp(cursor.getLong(timestampIndex));
        return pin;
    }

    /**
     * Gets the pin with required lat & lon
     *
     * @param latLon the lat lon
     * @return the map pin
     */
    public WeatherPin getMapPin(LatLng latLon) {
        WeatherPin pin = null;
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(
                TABLE_NAME,
                ALL_COLUMNS,
                LAT_LON_SELECTION,
                new String[] { String.valueOf(latLon.latitude), String.valueOf(latLon.longitude) },
                null,
                null,
                null
        );

        if (cursor.moveToFirst())
            pin = getMapPin(cursor);

        cursor.close();
        close();
        return pin;
    }

    /**
     * Gets all pins from DB
     *
     * @return the map pins list
     */
    public List<WeatherPin> getMapPins() {
        List<WeatherPin> pins = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(SQL_SELECT_ALL, null);

        if (cursor.moveToFirst())
            do {
                pins.add(getMapPin(cursor));
            } while (cursor.moveToNext());

        cursor.close();
        close();
        return pins;
    }

    /**
     * Gets the cursor for all pins
     *
     * @return the cursor
     */
    public Cursor getCursor() {
        SQLiteDatabase db = getReadableDatabase();
        return db.rawQuery(SQL_SELECT_ALL, null);
    }

    /**
     * Delete the pin from DB.
     *
     * @param latLon the lat & lon
     * @return the number of rows affected
     */
    public int deleteMapPin(LatLng latLon) {
        SQLiteDatabase db = getWritableDatabase();

        int deletedRows = db.delete(
                TABLE_NAME,
                LAT_LON_SELECTION,
                new String[] { String.valueOf(latLon.latitude), String.valueOf(latLon.longitude) }
        );

        close();
        return deletedRows;
    }

    /**
     * Delete map pin from DB on current cursor position
     *
     * @param cursor the cursor
     * @return the number of rows affected
     */
    public int deleteMapPin(Cursor cursor) {
        LatLng latLon = getMapPin(cursor).getLatLng();
        return deleteMapPin(latLon);
    }

    /**
     * Delete map pin from DB
     *
     * @param pinId the pin id
     * @return the number of rows affected
     */
    public int deleteMapPin(long pinId) {
        SQLiteDatabase db = getWritableDatabase();

        int deletedRows = db.delete(
                TABLE_NAME,
                PIN_ID_SELECTION,
                new String[] { String.valueOf(pinId) }
        );

        close();
        return deletedRows;
    }

}