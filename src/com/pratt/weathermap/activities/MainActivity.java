package com.pratt.weathermap.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.GoogleApiAvailability;
import com.pratt.weathermap.R;
import com.pratt.weathermap.fragments.NavigationDrawerFragment;
import com.pratt.weathermap.fragments.OnFragmentAttached;
import com.pratt.weathermap.fragments.OnPinsListItemCallbacks;
import com.pratt.weathermap.fragments.PinsListFragment;
import com.pratt.weathermap.fragments.WeatherMapFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks, OnFragmentAttached, OnPinsListItemCallbacks {

    /** Fragment managing the behaviors, interactions and presentation of the navigation drawer. */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /** The pins list fragment */
    private PinsListFragment mPinsListFragment;

    /** The weather map fragment. */
    private WeatherMapFragment mWeatherMapFragment;

    /** Used to store the last screen title. For use in {@link #restoreActionBar()}. */
    private String mTitle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPinsListFragment = PinsListFragment.newInstance();
        mWeatherMapFragment = WeatherMapFragment.newInstance();

        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle().toString();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
    }

    @Override
    public void onResume() {
        super.onResume();
        checkPlayServices();
    }

    private void checkPlayServices() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();

        int status = api.isGooglePlayServicesAvailable(this);
        if (api.isUserResolvableError(status)) {
            api.getErrorDialog(this, status, 1).show();
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Fragment fragment;

        switch (position) {
            case 0:
                fragment = mWeatherMapFragment;
                break;
            case 1:
                fragment = mPinsListFragment;
                break;
            default:
                throw new RuntimeException("There's just 2 positions in nav drawer.");
        }

        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
    }

    @Override
    public void onFragmentAttached(Fragment fragment) {
        if (fragment == mWeatherMapFragment)
            mTitle = getString(R.string.map_view_section);
        else if (fragment == mPinsListFragment)
            mTitle = getString(R.string.pins_list_section);
        else
            mTitle = getString(R.string.app_name);
    }

    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPinsListItemClicked(int position) {
        mNavigationDrawerFragment.selectItem(0);
        mWeatherMapFragment.updateCameraPositionAndSelectedMarker(position);
    }

    @Override
    public void onPinListItemDeleted(int position) {
        mWeatherMapFragment.removeMarkerFromMap(position);
    }

}
